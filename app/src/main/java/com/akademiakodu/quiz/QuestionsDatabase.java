package com.akademiakodu.quiz;

import java.util.List;

public interface QuestionsDatabase {
    List<Question> getQuestions(int difficulty);
}
